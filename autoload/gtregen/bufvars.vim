"▶1 
scriptencoding utf-8
execute frawor#Setup('0.0', {'@/resources': '0.0',}, 0)
let s:bufvars={}
"▶1 bufwipeout
function s:F.bufwipeout()
    let buf=+expand('<abuf>')
    if has_key(s:bufvars, buf)
        call filter(s:bufvars[buf].tree.buffers, 'v:val=='.buf)
        unlet s:bufvars[buf]
    endif
endfunction
augroup GtregenBufVars
    autocmd BufWipeOut * :call s:F.bufwipeout()
augroup END
let s:_augroups+=['GtregenBufVars']
"▶1
call s:_f.postresource('bufvars', s:bufvars, 1)
"▶1
call frawor#Lockvar(s:, 'bufvars')
" vim: ft=vim ts=4 sts=4 et fmr=▶,▲
