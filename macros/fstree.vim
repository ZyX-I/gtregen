scriptencoding utf-8
execute frawor#Setup('0.0', {'@gtregen': '0.0',
            \                    '@/os': '0.0',})
let s:_messages={
            \'notadir': 'Not a directory: %s',
        \}
let s:columns={}
function s:F.gennodes(tdata)
    let path=s:_r.os.path.join(a:tdata.path)
    if !s:_r.os.path.isdir(path)
        return []
    endif
    return map(copy(s:_r.os.listdir(path)),
                \'{"path": a:tdata.path+[v:val], '.
                \ '"nodes": s:F.gennodes}')
endfunction
function s:F.get(path)
    if !s:_r.os.path.isdir(a:path)
        call s:_f.throw('notadir', a:path)
    endif
    let tdata={}
    let tdata.path=s:_r.os.path.split(a:path)
    let tdata.nodes=s:F.gennodes
    return tdata
endfunction
function s:columns.size(tdata)
    let path=s:_r.os.path.join(a:tdata.path)
    return getfsize(path)
endfunction
function s:columns.permissions(tdata)
    let path=s:_r.os.path.join(a:tdata.path)
    return getfperm(path)
endfunction
function s:F.namecmp(a, b)
    let a=substitute(a:a, '\W', '', 'g')
    let b=substitute(a:b, '\W', '', 'g')
    return ((a is? b)?(0):((a>?b)?(1):(-1)))
endfunction
function s:F.sizecmp(a, b)
    let a=+a:a
    let b=+a:b
    return ((a==b)?(0):((a>b)?(1):(-1)))
endfunction
let s:mappings={}
let s:mappings.show={'lhs': 'p', 'selection': 'save'}
function s:mappings.show.function(bvar, node)
    echom join(a:node.path, s:_r.os.sep)
    return ''
endfunction
let s:mappings.showpath={'lhs': 'P'}
function s:mappings.showpath.function(bvar, node)
    echom string(a:node.path)
    return ''
endfunction
call s:_f.tree.add('fs', s:F.get, {'columns': s:columns,
            \                   'coldefault': ['name', 'size'],
            \                     'mappings': s:mappings,
            \                      'options': {'colopts':
            \                                   {'size':
            \                                       {'align': 'right',
            \                                        'title': 'Size',
            \                                       'talign': 'left',
            \                                          'cmp': s:F.sizecmp,},
            \                                    'name': {'cmp': s:F.namecmp},},
            \                                  'treechars': ' ╻╹┃╺┏┗┣╸┓┛┫━┳┻╋',
            \                                  'header': '# Header',
            \                                  'footer': "# Multiline\n# Footer",}})
call frawor#Lockvar(s:, '')
